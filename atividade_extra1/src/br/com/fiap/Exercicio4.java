package br.com.fiap;

public class Exercicio4 {
	
	public static void main(String[] args) {


		//operacao ==0 é soma
		//operacao ==1 é subtrair
		//operacao ==3 é multiplicar
		//operacao ==4 é dividir
		Exercicio4 calculosMatematicos = new Exercicio4();
		float soma = calculosMatematicos.realizarOperacao(10, 10, 0);
		float subtracao = calculosMatematicos.realizarOperacao(10, 10, 1);
		float multiplicacao = calculosMatematicos.realizarOperacao(10, 10, 2);
		float divisao = calculosMatematicos.realizarOperacao(10, 10, 3);

		System.out.println("Soma: " + soma);
		System.out.println("Subtracao: " + subtracao);
		System.out.println("Multiplicacao: " + multiplicacao);
		System.out.println("Divisão: " + divisao);
	}
	
	private float somar(float numero1, float numero2) {
		return numero1 + numero2;
	}
	
	private float multiplicar(float numero1, float numero2) {
		return numero1 * numero2;
	}
	
	private float dividir(float numero1, float numero2) {
		return numero1 / numero2;
	}
	
	private float subtrair(float numero1, float numero2) {
		return numero1 - numero2;
	}
	
	public float realizarOperacao(float numero1, float numero2, int operacao) {
		float resultado =0;
		if (operacao == 0) {
			resultado = this.somar(numero1, numero2);
		} else if (operacao == 1) {
			resultado = this.subtrair(numero1, numero2);
		} else if(operacao == 2) {
			resultado = this.multiplicar(numero1, numero2);
		} else if (operacao == 3) {
			resultado = this.dividir(numero1, numero2);
		}
		
		return resultado;
	}
}
