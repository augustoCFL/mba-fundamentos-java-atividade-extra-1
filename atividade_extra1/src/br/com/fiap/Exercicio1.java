package br.com.fiap;

public class Exercicio1 {

	public static void main(String[] args) {
		
		int [] array = {1,2,3};
		for(int element : retornarArrayInverso(array)) {
			System.out.println(element);
		}
		
	}
	
	private static int[] retornarArrayInverso(int [] array ) {
		
		int [] arrayInverso =  new int[array.length];
		int count = 0;
		for(int i =array.length; i> 0; i--) {
			arrayInverso[count] = array[i-1];
			count++;
		}
		
		return arrayInverso;
	}

}
