package br.com.fiap;

import javax.swing.JOptionPane;

public class Exercicio5 {
	
	public static void main(String[] args) {
		
		String numeroFatorialString = JOptionPane.showInputDialog("Digite o número que deseja calcular o fatorial");
		
		Integer numeroFatorial = Integer.valueOf(numeroFatorialString);
		
		Integer fatorial = numeroFatorial;
		for(int i = numeroFatorial; i> 1; i--) {
			
			fatorial = fatorial * (numeroFatorial-1);
			numeroFatorial = numeroFatorial -1;
			System.out.println(fatorial);
		}
	}
}
