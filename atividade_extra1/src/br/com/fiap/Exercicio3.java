package br.com.fiap;

import javax.swing.JOptionPane;

public class Exercicio3 {

	public static void main(String[] args) {
		String tamanhoArray = JOptionPane.showInputDialog("Digite a quantidade de salários que deseja cadastrar");

		double arraySalarios [] = new double[Integer.parseInt(tamanhoArray)];

		double soma = 0;
		for(int i =0; i< arraySalarios.length; i++) {

			String salario = JOptionPane.showInputDialog("Digite o salário do funcionário");

			arraySalarios[i] = Double.parseDouble(salario);
			soma = soma + Double.parseDouble(salario);
		}
		double media = soma / arraySalarios.length;
		JOptionPane.showMessageDialog(null, "Média dos salários: " + media);

	}

}
